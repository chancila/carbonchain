LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := libcarbonchain
LOCAL_CFLAGS    := -Werror
LOCAL_C_INCLUDES := \
	${LOCAL_PATH}/include
LOCAL_SRC_FILES := \
	src/CarbonChainJNI.cpp \
	src/nucleus/Engine.cpp \
	src/nucleus/Renderer.cpp \
	src/nucleus/Scene.cpp \
	src/nucleus/Texture.cpp \
	src/nucleus/Bitmap.cpp \
	src/nucleus/Shader.cpp \
	src/nucleus/ShaderProgram.cpp \
	src/nucleus/Vertex.cpp \
	src/nucleus/GLContext.cpp
	
LOCAL_LDLIBS    := -llog -lGLESv2

include $(BUILD_SHARED_LIBRARY)
