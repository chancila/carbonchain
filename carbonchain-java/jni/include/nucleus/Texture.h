#ifndef NUCLEUS_TEXTURE_H
#define NUCLEUS_TEXTURE_H

#include <nucleus/Lifecycle.h>
#include <GLES2/gl2.h>

namespace nucleus {
    class Bitmap;

    class Texture: public GLResource {
    public:
        Texture();
        Texture(Bitmap* bmp);
        virtual ~Texture();

        int getWidth() const;
        int getHeight() const;

        GLuint getTextureId() const;

        void setBitmap(Bitmap* bmp);

        Bitmap* getBitmap();

    protected:
        virtual void onLoad();
        virtual void onUnload();
    private:
        GLuint mTextureId;
        Bitmap* mBitmap;
        bool mTextureLoaded;

        void loadTextureToGL();
        void deleteTextureFromGL();
    };
}

#endif //end NUCLEUS_TEXTURE_H
