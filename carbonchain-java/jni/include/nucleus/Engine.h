#ifndef NUCLEUS_ENGINE_H
#define NUCLEUS_ENGINE_H

namespace nucleus {

    class Renderer;
    class Scene;

    class Engine {
    public:
        Engine(Renderer* rnd);
        virtual ~Engine();

        void pause();
        void start();

        void setActiveScene(Scene* scn);
        inline Scene* getActiveScene() const {
            return mCurrentScene;
        }

        inline Renderer* getRenderer() const {
            return mRenderer;
        }
    private:
        Renderer* mRenderer;
        Scene* mCurrentScene;
    };
}
#endif
