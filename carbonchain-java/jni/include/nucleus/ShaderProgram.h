#ifndef NUCLEUS_PROGRAM_H_
#define NUCLEUS_PROGRAM_H_

#include <nucleus/Lifecycle.h>
#include <GLES2/gl2.h>

namespace nucleus {
    class Shader;

    class ShaderProgram: public GLResource {
    public:
        ShaderProgram(Shader* vertex, Shader* fragment);

        inline GLuint getId() const {
            return mProgramId;
        }
    protected:
        virtual void onLoad();
        virtual void onUnload();
    private:
        GLuint mProgramId;
        Shader* mVertex;
        Shader* mFragment;

        void loadToGL();
        void deleteFromGL();
    };
}

#endif /* NUCLEUS_PROGRAM_H_ */
