#ifndef NUCLEUS_SCENE
#define NUCLEUS_SCENE

#include <glm/glm.hpp>
#include <list>

namespace nucleus {

    class Layer;
    class Camera;
    class Renderer;

    class Scene {
    public:
        Scene(Camera* camera);
        virtual ~Scene();

        virtual void init();
        virtual void destroy();

        virtual void pause();
        virtual void start();

        virtual void draw(Renderer& rnd);

        inline void setViewportDimensions(const glm::ivec2& dims) {
            mViewportDimensions = dims;
        }
        inline const glm::ivec2& getViewportDimensions() const {
            return mViewportDimensions;
        }

        void addLayerToFront(Layer* layer);
        void addLayerToBack(Layer* layer);
        void deleteLayer(Layer* layer);

        void moveLayerToFront(Layer* layer);
        void moveLayerToBack(Layer* layer);

        inline Camera* getCamera() {
            return mCamera;
        }

    private:
        glm::ivec2 mViewportDimensions;
        std::list<Layer*> mLayers;
        Camera* mCamera;
    };
}

#endif
