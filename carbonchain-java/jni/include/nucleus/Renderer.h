#ifndef NUCLEUS_RENDERER_H_
#define NUCLEUS_RENDERER_H_

#include <glm/glm.hpp>
#include <nucleus/Lifecycle.h>

namespace nucleus {

    class Scene;
    class Texture;
    class ShaderProgram;
    class VertexBuffer;

    class Renderer {
    public:
        enum BufferType {
            BUFFER_COLOR = 1, BUFFER_DEPTH = 2, BUFFER_STENCIL = 4
        };

        Renderer();
        virtual ~Renderer();
        void init();
        void destroy();

        void pause();
        void start();

        void setClearColor(float r, float g, float b, float a = 1.0f);
        void setClearDepth(float d);
        void setClearStencil(int d);

        void clearColorWith(float r, float g, float b, float a = 1.0f);

        void clear(BufferType what);

        void bindTexture(Texture* txt);
        inline Texture* getActiveTexture() {
            return mActiveTexture;
        }

        void bindShaderProgram(ShaderProgram* shader);
        inline ShaderProgram* getActiveShader() {
            return mActiveShader;
        }

        void bindVBO(VertexBuffer* buffer);
        inline VertexBuffer* getActiveVBO() {
            return mActiveVBO;
        }
    private:
        Texture* mActiveTexture;
        VertexBuffer* mActiveVBO;
        ShaderProgram* mActiveShader;
        glm::ivec2 mViewport;
    };
}

// namespace nucleus
#endif /* NUCLEUS_RENDERER_H_ */
