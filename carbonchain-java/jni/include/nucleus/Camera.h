#ifndef NUCLEUS_CAMERA_H
#define NUCLEUS_CAMERA_H

#include <glm/glm.hpp>

namespace nucleus {
    class Camera {
    public:
        virtual ~Camera();
        virtual void setPosition(int x, int y, int z = 0);
        virtual void pan(int x, int y);
        virtual glm::mat4x4 getModelViewMatrix() const;
    private:
        glm::vec3 mPosition;
    };
}
#endif
