#ifndef NUCLEUS_SHADER_H
#define NUCLEUS_SHADER_H

#include <nucleus/Lifecycle.h>
#include <GLES2/gl2.h>

namespace nucleus {
    class Shader: public GLResource {
    public:
        enum Type {
            VERTEX, FRAGMENT
        };

        Shader(const Type& type,
               const char* str,
               const void* data,
               int dataLength);

        inline const Type& getType() const {
            return mType;
        }

        inline GLuint getId() const {
            return mShaderId;
        }

        virtual ~Shader();

    protected:
        virtual void onLoad();
        virtual void onUnload();
    private:
        void loadToGL();
        void deleteFromGL();

        GLuint mShaderId;
        const char* mSourceData;

        const void* mBinaryData;
        int mBinaryDataLength;

        Type mType;
    };
}
#endif //NUCLEUS_SHADER_H
