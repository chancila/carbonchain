#ifndef NUCLEUS_LIFECYCLE_H
#define NUCLEUS_LIFECYCLE_H

namespace nucleus {
    class GLResource {
    public:
        enum State {
            UNLOADED, LOADED
        };

        GLResource() :
                mState(UNLOADED) {
        }

        virtual ~GLResource() {
        }

        inline void load() {
            if (UNLOADED) {
                onLoad();
                mState = LOADED;
            }
        }

        inline void unload() {
            if (LOADED) {
                onLoad();
            }

            mState = UNLOADED;
        }

        const State& getState() const {
            return mState;
        }

        void setState(State& state) {
            mState = state;
        }

    protected:
        virtual void onLoad() = 0;
        virtual void onUnload() = 0;
    private:
        State mState;
    };
}
#endif
