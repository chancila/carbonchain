#ifndef LAYER_H_
#define LAYER_H_
#include <nucleus/Point.h>

namespace nucleus {
class Layer {
public:

	inline void setDimensions(const Point2D<int> dims) { mDimensions = dims; }
	inline const Point2D<int>& getDimensions() const { return mDimensions; }
	bool hasTransparency() const { return mIsTransparent; };

private:
	Point2D<int> mDimensions;
	bool mIsTransparent;
};
}

#endif /* LAYER_H_ */
