#ifndef NUCLEUS_VERTEX_H_
#define NUCLEUS_VERTEX_H_

#include <nucleus/Lifecycle.h>
#include <GLES2/gl2.h>
#include <glm/glm.hpp>
#include <cstring>
namespace nucleus {

    struct vertex2d_simple {
        float position[2];
    };

    struct vertex2d {
        float position[2];
        float uv[2];
    };

    struct vertex3d {
        float position[3];
        float uv[2];
        float normal[3];
    };

    class VertexBuffer: public GLResource {
    public:
        template<class T, class U>
        VertexBuffer(T* vertexData,
                     int vertexDataLength,
                     U* indexData,
                     int indexDataLength,
                     GLenum usage = GL_STATIC_DRAW) :
                mUsage(usage) {
            setData(vertexData, vertexDataLength, indexData, indexDataLength);
        }
        virtual ~VertexBuffer();

        template<class T, class U>
        void setData(T* vertexData,
                     int vertexDataLength,
                     U* indexData,
                     int indexDataLength) {
            mVertexDataSize = vertexDataLength * sizeof(T);
            mVertexData = malloc(mVertexDataSize);

            mIndexDataSize = indexDataLength * sizeof(U);
            mIndexData = malloc(mIndexDataSize);

            memcpy(mVertexData, vertexData, mVertexDataSize);
            memcpy(mIndexData, indexData, mIndexDataSize);

            if (getState() == LOADED) {
                uploadData();
            }
        }

        inline GLuint getVertexBufferId() const {
            return mVertexBufferId;
        }

        inline GLuint getIndexBufferId() const {
            return mIndexBufferId;
        }
    protected:
        virtual void onLoad();
        virtual void onUnload();
    private:
        void loadToGL();
        void deleteFromGL();
        void uploadData();

        GLenum mUsage;
        GLuint mVertexBufferId, mIndexBufferId;

        void* mVertexData;
        void* mIndexData;

        size_t mVertexDataSize;
        size_t mIndexDataSize;
    };
}

#endif /* NUCLEUS_VERTEX_H_ */
