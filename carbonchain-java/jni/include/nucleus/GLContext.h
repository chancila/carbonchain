#ifndef NUCLEUS_GLCONTEXT_H_
#define NUCLEUS_GLCONTEXT_H_

#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

namespace nucleus {
    class GLContext {
    public:
        GLContext();
        virtual ~GLContext();
        void contextGained();
        void contextLost();

        inline bool isInitialized() {
            return mInitialized;
        }

        inline bool isLost() {
            return mLost;
        }

        bool oes_texture_npot;
        bool oes_get_program_binary;

        bool img_shader_binary;
        bool img_program_binary;

        bool oes_rgb8_rgba8;
    private:
        void onFirstContextLoad();
        bool mInitialized;
        bool mLost;
    public:
        static void logShaderInfoLog(GLuint shaderid, const char* heading);
        static void logProgramInfoLog(GLuint programid,const char* heading);
        static void checkGLError(const char* op);
        static void printGLString(const char *name, GLenum s);
    };
} // namespace nucleus
#endif /* NUCLEUS_GLCONTEXT_H_ */
