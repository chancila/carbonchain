#ifndef NUCLEUS_BITMAP_H
#define NUCLEUS_BITMAP_H

namespace nucleus {
    class Bitmap {
    public:
        enum Format {
            RGB565, RGB888, RGBA8888, A8
        };
        Bitmap(int width, int height, void* data, Format format);
        Bitmap();

        void setData(int width, int height, void* data, Format format);

        inline int getWidth() const {
            return mWidth;
        }
        inline int getHeight() const {
            return mHeight;
        }

        inline void* getData() {
            return mData;
        }

        inline const Format& getFormat() const {
            return mFormat;
        }

    private:
        void* mData;
        int mWidth;
        int mHeight;
        Format mFormat;
    };
}
#endif //#{symbol}
