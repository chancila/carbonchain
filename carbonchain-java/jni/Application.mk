# The ARMv7 is significanly faster due to the use of the hardware FPU
APP_ABI := armeabi armeabi-v7a
APP_STL := stlport_shared
APP_PLATFORM := android-9
APP_CFLAGS := -DANDROID
APP_CPPFLAGS := \
	-fno-exceptions \
	-D_STLP_NO_WCHAR_T