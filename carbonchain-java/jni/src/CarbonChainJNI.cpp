#include <jni.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <nucleus/Logging.h>
#include <nucleus/Engine.h>
#include <nucleus/Camera.h>
#include <nucleus/Renderer.h>
#include <nucleus/Scene.h>

#include <glm/glm.hpp>

using namespace nucleus;

#define  LOG_TAG    "CarbonChainJNI"
#define LOG_DRAW_FRAME 0

Engine* gEngine;
Scene* gScene;

extern "C" {
    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_init(JNIEnv * env,
                                                                        jobject obj);
    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_destroy(JNIEnv * env,
                                                                           jobject obj);

    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_pause(JNIEnv * env,
                                                                         jobject obj);
    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_resume(JNIEnv * env,
                                                                          jobject obj);

    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setDimensions(JNIEnv * env,
                                                                                 jobject obj,
                                                                                 jint width,
                                                                                 jint height);

    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setViewportSize(JNIEnv * env,
                                                                                   jobject obj,
                                                                                   jint width,
                                                                                   jint height);
    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setViewportPosition(JNIEnv * env,
                                                                                       jobject obj,
                                                                                       jint x,
                                                                                       jint y);

    JNIEXPORT void
    JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_draw(JNIEnv * env,
                                                                        jobject obj);
}
;

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_init(JNIEnv * env,
                                                                                   jobject obj) {
    LOGV("Initializing");
    if (gEngine == NULL) {
        gEngine = new Engine(new Renderer());

        gScene = 0; //TODO set scene here
//        gEngine->setActiveScene(gScene);
    }
}
JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_destroy(JNIEnv * env,
                                                                                      jobject obj) {
    LOGV("Destroying");
    if (gEngine) {
        delete gEngine;
        gEngine = NULL;

        if (gScene) {
            delete gScene;
            gScene = NULL;
        } else {
            LOGW("Trying to destroy uninitalized scene");
        }
    } else {
        LOGW("Trying to destroy uninitalized context");
    }
}

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_pause(JNIEnv * env,
                                                                                    jobject obj) {
    LOGV("Pausing");
    if (gEngine) {
        gEngine->pause();
    } else {
        LOGE("Trying to pause without init");
    }
}
JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_resume(JNIEnv * env,
                                                                                     jobject obj) {
    LOGV("Resuming");
    if (gEngine) {
        gEngine->start();
    } else {
        LOGE("Trying to resume without init");
    }
}

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setDimensions(JNIEnv * env,
                                                                                            jobject obj,
                                                                                            jint width,
                                                                                            jint height) {
    LOGV("Setting dimensions %d %d", width, height);

}

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setViewportSize(JNIEnv * env,
                                                                                              jobject obj,
                                                                                              jint width,
                                                                                              jint height) {
    LOGV("Setting viewport size %d %d", width, height);
    if (gScene) {
        gScene->setViewportDimensions(glm::ivec2(width,height));
    } else {
        LOGW("No scene exists");
    }
}

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_setViewportPosition(JNIEnv * env,
                                                                                                  jobject obj,
                                                                                                  jint x,
                                                                                                  jint y) {
    LOGV("Setting viewport position %d %d", x, y);
    if (gScene) {
        gScene->getCamera()->setPosition(x, y);
    } else {
        LOGW("No scene exists");
    }
}

JNIEXPORT void JNICALL Java_com_eightbitshift_glwallpaper_CarbonChainRenderer_draw(JNIEnv * env,
                                                                                   jobject obj) {
    if (LOG_DRAW_FRAME)
        LOGV("Drawing frame");

    if (gEngine) {
        gEngine->getRenderer()->clearColorWith(1, 0, 0);
//        gScene->draw(*gEngine->getRenderer());
    } else {
        LOGW("%s: No renderer exists", __func__);
    }
}

