#include <nucleus/Texture.h>
#include <nucleus/Bitmap.h>
#include <nucleus/Logging.h>

#define LOG_TAG "NucleusTexture"

using namespace nucleus;

static GLint getGlTextureFormat(Bitmap::Format format) {
    switch (format) {
        case Bitmap::RGB565:
            return GL_RGB;
        case Bitmap::RGB888:
            return GL_RGB;
        case Bitmap::RGBA8888:
            return GL_RGBA;
    }
}
static GLint getGlTextureType(Bitmap::Format format) {
    switch (format) {
        case Bitmap::RGB565:
            return GL_UNSIGNED_SHORT_5_6_5;
        case Bitmap::RGB888:
            return GL_UNSIGNED_BYTE;
        case Bitmap::RGBA8888:
            return GL_UNSIGNED_BYTE;
    }

}

nucleus::Texture::Texture() :
        GLResource(),
        mTextureId(-1),
        mTextureLoaded(false) {
}

nucleus::Texture::Texture(Bitmap* bmp) :
        GLResource(),
        mTextureId(-1),
        mTextureLoaded(false) {
    setBitmap(bmp);
}

nucleus::Texture::~Texture() {
}

int nucleus::Texture::getWidth() const {
    if (mBitmap)
        return mBitmap->getWidth();
    else
        return -1;
}

int nucleus::Texture::getHeight() const {
    if (mBitmap)
        return mBitmap->getHeight();
    else
        return -1;
}

GLuint nucleus::Texture::getTextureId() const {
    return mTextureId;
}

void nucleus::Texture::loadTextureToGL() {
    deleteTextureFromGL();
    glGenTextures(1, &mTextureId);
    glBindTexture(GL_TEXTURE_2D, mTextureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GLint format = getGlTextureFormat(mBitmap->getFormat());
    GLint type = getGlTextureType(mBitmap->getFormat());
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 format,
                 mBitmap->getWidth(),
                 mBitmap->getHeight(),
                 0,
                 format,
                 type,
                 mBitmap->getData());
    mTextureLoaded = true;
}

void nucleus::Texture::deleteTextureFromGL() {
    if (!mTextureLoaded) {
        LOGD("%s: No texture loaded for deletion", __func__);
        return;
    }

    glDeleteTextures(1, &mTextureId);
    mTextureId = -1;
}

void nucleus::Texture::setBitmap(Bitmap *bmp) {
    mBitmap = bmp;
    if (getState() >= LOADED) {
        loadTextureToGL();
    }
}

nucleus::Bitmap* nucleus::Texture::getBitmap() {
    return mBitmap;
}

void nucleus::Texture::onLoad() {
    if (mBitmap) {
        loadTextureToGL();
    }
}

void nucleus::Texture::onUnload() {
    deleteTextureFromGL();
}

