#include <nucleus/Bitmap.h>

nucleus::Bitmap::Bitmap(int width, int height, void *data, Format format) {
    setData(width, height, data, format);
}

nucleus::Bitmap::Bitmap() :
        mData(0) {
}

void nucleus::Bitmap::setData(int width,
                              int height,
                              void *data,
                              Format format) {
    mWidth = width;
    mHeight = height;
    mData = data;
    mFormat = format;
}
