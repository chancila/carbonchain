#include <nucleus/ShaderProgram.h>
#include <nucleus/Logging.h>
#include <nucleus/Shader.h>
#include <nucleus/GLContext.h>

#define LOG_TAG "NucleusShaderProgram"

nucleus::ShaderProgram::ShaderProgram(Shader *vertex, Shader *fragment) :
        mVertex(vertex),
        mFragment(fragment) {
}

void nucleus::ShaderProgram::onLoad() {
    loadToGL();
}

void nucleus::ShaderProgram::onUnload() {
    deleteFromGL();
}

void nucleus::ShaderProgram::loadToGL() {
    mProgramId = glCreateProgram();
    if (mProgramId == 0) {
        GLContext::checkGLError("glCreateProgram");
        return;
    }

    glAttachShader(mProgramId, mVertex->getId());
    GLContext::checkGLError("glAttachShader");
    glAttachShader(mProgramId, mFragment->getId());
    GLContext::checkGLError("glAttachShader");
    glLinkProgram(mProgramId);
    GLContext::checkGLError("glAttachShader");

    int status;
    glGetProgramiv(mProgramId, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
        GLContext::logProgramInfoLog(mProgramId, "Failed to link program");
        return;
    }
}

void nucleus::ShaderProgram::deleteFromGL() {
    glDeleteProgram(mProgramId);
}

