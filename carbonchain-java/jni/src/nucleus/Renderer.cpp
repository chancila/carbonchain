#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2platform.h>

#include <nucleus/Logging.h>
#include <nucleus/Renderer.h>
#include <nucleus/Texture.h>
#include <nucleus/ShaderProgram.h>
#include <nucleus/Vertex.h>
#include <nucleus/GLContext.h>

using namespace nucleus;

#define LOG_TAG "NucleusRenderer"

static GLint getMaskForBufferType(int what) {
    GLint glType = 0;
    switch (what) {
        case Renderer::BUFFER_COLOR & 0x01:
            glType |= GL_COLOR_BUFFER_BIT;
            break;
        case Renderer::BUFFER_DEPTH & 0x02:
            glType |= GL_DEPTH_BUFFER_BIT;
            break;
        case Renderer::BUFFER_STENCIL & 0x04:
            glType |= GL_STENCIL_BUFFER_BIT;
            break;
        default:
            return -1;
    }

    return glType;
}

static GLint getClearValueForBufferType(Renderer::BufferType what) {
    switch (what) {
        case Renderer::BUFFER_COLOR:
            return GL_COLOR_CLEAR_VALUE;
        case Renderer::BUFFER_DEPTH:
            return GL_DEPTH_CLEAR_VALUE;
        case Renderer::BUFFER_STENCIL:
            return GL_STENCIL_CLEAR_VALUE;
        default:
            return -1;
    }
}

nucleus::Renderer::Renderer() :
        mActiveTexture(0),
        mActiveShader(0) {
}

nucleus::Renderer::~Renderer() {
}

void nucleus::Renderer::init() {
}

void nucleus::Renderer::destroy() {
}

void nucleus::Renderer::pause() {
}

void nucleus::Renderer::start() {

}

void nucleus::Renderer::setClearColor(float r, float g, float b, float a) {
    glClearColor(r, g, b, a);
}

void nucleus::Renderer::setClearDepth(float d) {
    glClearDepthf(d);
}

void nucleus::Renderer::setClearStencil(int d) {
    glClearStencil(d);
}

void nucleus::Renderer::clear(BufferType what) {
    glClear(getMaskForBufferType(what));
}

void nucleus::Renderer::bindTexture(Texture *txt) {
    if (mActiveTexture != txt) {
        mActiveTexture = txt;
        if (txt != 0) {
            glBindTexture(GL_TEXTURE_2D, txt->getTextureId());
        }
    }
}

void nucleus::Renderer::clearColorWith(float r, float g, float b, float a) {
    float prev[4];
    glGetFloatv(GL_COLOR_BUFFER_BIT, prev);
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(prev[0], prev[1], prev[2], prev[3]);
}

void nucleus::Renderer::bindShaderProgram(ShaderProgram *shader) {
    if (mActiveShader != shader) {
        mActiveShader = shader;
        GLuint shaderId;
        if (shader == 0) {
            shaderId = 0;
        } else {
            shaderId = shader->getId();
        }

        glUseProgram(shaderId);
        GLContext::checkGLError("glUseProgram");
    }
}

void nucleus::Renderer::bindVBO(VertexBuffer *buffer) {
    glBindBuffer(GL_ARRAY_BUFFER, buffer->getVertexBufferId());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->getIndexBufferId());
}

