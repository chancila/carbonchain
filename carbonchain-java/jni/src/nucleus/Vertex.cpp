#include <nucleus/Vertex.h>

void nucleus::VertexBuffer::loadToGL() {
    GLuint ids[2];
    glGenBuffers(2, ids);

    mVertexBufferId = ids[0];
    mIndexBufferId = ids[1];

    uploadData();
}

void nucleus::VertexBuffer::onLoad() {
    loadToGL();
}

void nucleus::VertexBuffer::onUnload() {
    deleteFromGL();
}

nucleus::VertexBuffer::~VertexBuffer() {
    free(mVertexData);
    free(mIndexData);
}

void nucleus::VertexBuffer::deleteFromGL() {
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void nucleus::VertexBuffer::uploadData() {
    int prevArray, prevIndex;

    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &prevArray);
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &prevIndex);

    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVertexBufferId);

    glBufferData(GL_ARRAY_BUFFER, mVertexDataSize, mVertexData, mUsage);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexDataSize, mIndexData, mUsage);

    glBindBuffer(GL_ARRAY_BUFFER, prevArray);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prevIndex);
}
