#include <nucleus/Shader.h>
#include <GLES2/gl2ext.h>
#include <nucleus/Logging.h>
#include <nucleus/GLContext.h>

#define LOG_TAG "NucleusShader"

using namespace nucleus;

nucleus::Shader::Shader(const Type & type,
                        const char *str,
                        const void *data,
                        int dataLength) :
        mType(type),
        mSourceData(str),
        mBinaryData(data),
        mBinaryDataLength(dataLength) {
}

nucleus::Shader::~Shader() {
}

void nucleus::Shader::onLoad() {
    loadToGL();
}

void nucleus::Shader::onUnload() {
    deleteFromGL();
}

void nucleus::Shader::loadToGL() {
    GLenum glType;

    switch (mType) {
        case VERTEX:
            glType = GL_VERTEX_SHADER;
            break;
        case FRAGMENT:
            glType = GL_FRAGMENT_SHADER;
            break;
    }

    mShaderId = glCreateShader(glType);

    if (mShaderId == 0) {
        LOGE("Failed to load shader");
        return;
    }

    if (mSourceData) {
        glShaderSource(mShaderId, 1, &mSourceData, 0);
        glCompileShader(mShaderId);

        int status;
        glGetShaderiv(mShaderId, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE) {
            GLContext::logShaderInfoLog(mShaderId, "Shader failed to compile");
        }
    } else if (mBinaryData) {

    } else {
        LOGE("No source or binary present for loading of shader");
        return;
    }
}

void nucleus::Shader::deleteFromGL() {
    glDeleteShader(mShaderId);
}

