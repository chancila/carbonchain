#include <nucleus/GLContext.h>
#include <cstring>
#include <nucleus/Logging.h>

#define LOG_TAG "NucleusGLContext"

static bool hasExtension(const char* extStr, const char* extName) {
    return strstr(extStr, extName);
}

nucleus::GLContext::GLContext() :
        mInitialized(false),
        mLost(true) {
}

nucleus::GLContext::~GLContext() {
}
void nucleus::GLContext::contextGained() {
    if (!mInitialized) {
        onFirstContextLoad();
        mInitialized = true;
    }
}

void nucleus::GLContext::contextLost() {
}

void nucleus::GLContext::onFirstContextLoad() {
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);
    printGLString("Shading Language Version", GL_SHADING_LANGUAGE_VERSION);

    const char* ext = (char*) glGetString(GL_EXTENSIONS);

    oes_texture_npot = hasExtension(ext, "GL_OES_texture_npot");
    oes_get_program_binary = hasExtension(ext, "GL_OES_get_program_binary");
    img_shader_binary = hasExtension(ext, "GL_IMG_shader_binary");
    img_program_binary = hasExtension(ext, "GL_IMG_program_binary");
    oes_rgb8_rgba8 = hasExtension(ext, "GL_OES_rgb8_rgba8");

}

void nucleus::GLContext::logShaderInfoLog(GLuint shaderid,
                                          const char *heading) {
    int length;
    glGetShaderiv(shaderid, GL_INFO_LOG_LENGTH, &length);

    char* log = new char[length];
    glGetShaderInfoLog(shaderid, length, 0, log);

    LOGD("%s", heading);
    LOGD("%s", log);

    delete[] log;
}

void nucleus::GLContext::checkGLError(const char *op) {
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGD("after %s() glError (0x%x)\n", op, error);
    }
}

void nucleus::GLContext::logProgramInfoLog(GLuint programid,
                                           const char *heading) {
    int length;
    glGetProgramiv(programid, GL_INFO_LOG_LENGTH, &length);

    char* log = new char[length];
    glGetProgramInfoLog(programid, length, 0, log);

    LOGD("%s", heading);
    LOGD("%s", log);

    delete[] log;
}

void nucleus::GLContext::printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGV("GL %s = %s\n", name, v);
}

