#include <nucleus/Engine.h>
#include <nucleus/Renderer.h>
#include <nucleus/Scene.h>

nucleus::Engine::Engine(Renderer *rnd) :
        mRenderer(rnd),
        mCurrentScene(0) {
    mRenderer->init();
}

nucleus::Engine::~Engine() {
    mRenderer->destroy();
    delete mRenderer;
}

void nucleus::Engine::pause() {
    if (mCurrentScene) {
        mCurrentScene->pause();
    }

    mRenderer->pause();
}

void nucleus::Engine::start() {
    mRenderer->start();

    if (mCurrentScene) {
        mCurrentScene->start();
    }
}

void nucleus::Engine::setActiveScene(Scene *scn) {
    if (mCurrentScene) {
        mCurrentScene->destroy();
    }

    mCurrentScene = scn;
    mCurrentScene->init();
}
