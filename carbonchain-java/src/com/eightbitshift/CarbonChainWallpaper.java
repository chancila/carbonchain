package com.eightbitshift;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import org.jf.GLWallpaper.GLWallpaperService;

import android.opengl.GLSurfaceView;
import android.util.Log;

import com.eightbitshift.glwallpaper.CarbonChainRenderer;

public class CarbonChainWallpaper extends GLWallpaperService {
    private static final String TAG            = "CarbonChainWallpaper";
    private static final int    DEBUG_GL_FLAGS = GLSurfaceView.DEBUG_CHECK_GL_ERROR;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Engine onCreateEngine() {

        return new CarbonChainEngine();
    }

    public class CarbonChainEngine extends GLEngine {

        public CarbonChainEngine() {
            Log.v(TAG, "Creating new carbon chain wallpaper engine");

            setEGLContextClientVersion(2);
            setEGLConfigChooser(5, 6, 5, 0, 8, 0);
            setDebugFlags(DEBUG_GL_FLAGS);
            
            setRenderer(new GLSurfaceView.Renderer() {

                @Override
                public void onSurfaceCreated(GL10 gl, EGLConfig config) {
                    CarbonChainRenderer.init();
                }

                @Override
                public void onSurfaceChanged(GL10 gl, int width, int height) {
                    CarbonChainRenderer.setViewportSize(width, height);
                }

                @Override
                public void onDrawFrame(GL10 gl) {
                    CarbonChainRenderer.draw();
                }
            });

            setRenderMode(GLWallpaperService.GLEngine.RENDERMODE_CONTINUOUSLY);

        }
    }
}
