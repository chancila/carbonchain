package com.eightbitshift.glwallpaper;

public class CarbonChainRenderer {
    static {
        System.loadLibrary("stlport_shared");
        System.loadLibrary("carbonchain");
    }

    public static native void init();

    public static native void destroy();

    public static native void pause();

    public static native void resume();

    public static native void setDimensions(int width, int height);

    public static native void setViewportSize(int width, int height);

    public static native void setViewportPosition(int x, int y);

    public static native void draw();
}
